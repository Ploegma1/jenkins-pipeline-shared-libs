def call(String contains) {
    lastCommitMsg = sh(script: "git --no-pager log-1 --pretty=%B", returnStdout: true)
    if (lastCommitMsg != null && lastCommitMsg.contains(contains)) {
        currentBuild.result = 'ABORTED'
        error("Stopping build, commit message contained ${contains}")
    }
}