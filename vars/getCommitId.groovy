def call() {
    commit_id = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
    echo "CommitID: ${commit_id}"
    return commit_id
}